# LK Informatik

Unterrichtsmaterialien für den Leistungskurs Informatik in der Gymnasialen Oberstufe am Berliner Gymnasium

Programmiersprachen: JavaScript, Haskell
Datenbanken: relational, MySQL

empfohlene Software für Windows oder Linux:
- [Firefox](https://www.mozilla.org/de/firefox/new/) oder [Chrome](https://www.google.com/chrome/)
- [Notepad++](https://notepad-plus-plus.org/downloads/) Text-Editor mit Syntax-Highlighting
- [LibreOffice](https://www.libreoffice.org/download/download/)
- [Dia](http://dia-installer.de/download/index.html.de) Grafikprogramm (z.B. für ERD oder UML)
