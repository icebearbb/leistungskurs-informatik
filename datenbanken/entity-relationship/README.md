# Grundlagen Entity Relationship Modell

Ein ER-Modell veranschaulicht Beziehungen (Relationen) zwischen Mengen "interessanter" Dinge (Entitäten). Es kann zur Planung des Schemas einer relationalen Datenbank genutzt werden.

Den Vorgang der Planung und Erstellung eines Schemas nennt man **Daten-Modellierung**.

![N|Solid](erd-abstrakt-einfach.png)

In der obigen Abbildung sind die Grundlagen des eines Entity-Relationship-Diagramms (ERD) dargestellt.

### Entität
Eine Entität bezeichnet eine Menge "interessanter", gleichartiger Dinge, z.B. Kunden, Produkte, Aufträge etc. Ein genau bestimmtes Ding als Element einer Menge nennt man **Ausprägung**.

### Attribut
Die Datenverarbeitung wichtigen Eigenschaften einer Entität bezeichnet man als Attribute. Mindestens eines der Attribute muss ein **Schlüsselattribut** sein, das jede Ausprägung einer Entität eindeutig bestimmt.