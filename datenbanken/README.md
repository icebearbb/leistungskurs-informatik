
# Grundlagen relationaler Datenbanken

## Definition Datenbank
Eine [Datenbank](https://de.wikipedia.org/wiki/Datenbank) ist ein System zur elektronischen Datenverwaltung.

[Daten](https://de.wikipedia.org/wiki/Daten) sind ([binär](https://games.penjee.com/binary-numbers-game/)) kodierte [Informationen](https://de.wikipedia.org/wiki/Information).

Ein **Datenbanksystem (DBS)** besteht aus zwei Teilen:
- **[Datenbasis](https://de.wikipedia.org/wiki/Datenbasis)** (DB, auch Datenbank genannt, Vorsicht!)
  - Die Struktur (der Aufbau) der Datenbasis wird durch das [Datenbank-Schema](https://www.lucidchart.com/pages/de/was-ist-ein-datenbankschema) bestimmt.
- **[Datenbankmanagementsystem](https://www.bigdata-insider.de/was-ist-ein-datenbankmanagementsystem-a-615034/)**: DBMS, eine Software zur Verwaltung der Datenbasis

Das Datenbankmanagementsystem erfüllt die folgenden Aufgaben:
- grundlegende Operationen auf der Datenbasis:
  - Daten auswählen und abrufen (*lesen*)
  - Daten erzeugen, ändern und löschen (*schreiben*)
- Zugriffssteuerung für gleichzeitigen Zugriff vieler Nutzer 
- Rechteverwaltung damit nicht jeder Nutzer auf alle Daten zugreifen darf (Datenschutz)
- Kodierung der Informationen (Stichwort [Datentypen](https://oracleplsql.info/datentypen-mysql.html), s.u.)
- Prüfung der Integrität ("richtiger Zusammenhang") der Daten
- Datensicherheit, z.B. regelmäßiges Backup
- Kommunikation mit Nutzern oder Maschinen über eine [Datenbanksprache](https://de.wikipedia.org/wiki/Data_Manipulation_Language)

> gebräuchliche Datentypen in relationalen Datenbanksystemen sind:
> - INTEGER: ganze Zahlen, 
> - FLOAT und DOUBLE: Kommazahlen, auch Fließkommazahl genannt, 
> - VARCHAR: Zeichenketten mit fester maximaler Länge, 
> - TEXT: Zeichenketten mit beliebiger Länge, 
> - DATE und TIME: Datum und Zeit.

## Relationenbegriff
[![N|Solid](https://upload.wikimedia.org/wikipedia/commons/0/0f/Relationen.svg)](https://de.wikipedia.org/wiki/Relation_(Mathematik))

Als Relation wird in der Mathematik die **Beziehung** zwischen zwei **Dingen** beschrieben. Dabei besteht zwischen zwei Dingen entweder eine Beziehung oder nicht. Die Dinge sind jeweils in verschiedenen **Mengen** zusammengefasst.

Ein Beispiel: Gegeben sei eine Reihe von Filmvorführungen in einem Kino und eine Menge von Personen. Besucht eine Person eine Vorstellung, so entsteht zwischen ihr und der Vorstellung eine Beziehung: Person a *besucht* Vorstellung y (siehe Grafik).

## Entity-Relationship-Modell




ER-Diagramme (ERD) können mit [Dia](http://dia-installer.de/download/index.html.de) oder LibreOffice Draw erstellt werden.

## relationales Schema

## Normalformen

## Normalisierung

## SQL

### CREATE TABLE

### INSERT

### UPDATE

### DELETE

### SELECT

#### Aggregationen

#### Abfragen über mehrere Tabellen (JOIN)

